#!/bin/sh

set -o errexit
set -o pipefail
set -o nounset


# Make initial migrations
python manage.py makemigrations

# Migrating to DB
python3 manage.py wait_for_db
python manage.py migrate

python manage.py runserver 0.0.0.0:8000

# CHANGELOG

## [v22.3.0] - 2021-01-27

### Added
- Added `add_player` action to `Team` CRUD
- Added testing for this new action

### Changed
### Fixed

## [v22.2.0] - 2021-01-27

### Added
- Added `count_goals` to `Team` CRUD
- Enabled `searching` by query_params for `GET` actions

### Changed
### Fixed
- gitlab-ci

## [v22.1.0] - 2021-01-27
First release to master that contains Team CRUD endpoints and Player CRUD endpoint

### Added
- Team CRUD
- Team CRUD endpoints testing
- Player CRUD
- Player CRUD endpoints testing
### Changed
None
### Fixed
None
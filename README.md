# Ejercicio 2 - Postulación Python Backend Developer
El presente proyecto representa una solución basado en los requerimientos especificados en el eniunciado del presente archivo. Toda la información relacionada con el desarrollo de este proyecto ha sido documentada en la carpeta `docs/`. Dentro de esta carpeta, es posible encontrar detalles de implementación del servidor web (`server/`), detalles de la estructura de las diferentes aplicación y carpetas (`app/`), y sobre el desarrollo de cada tarea basado en los requerimientos iniciales (`diary/`).

# Enunciado
Existen dos entidades:
- Jugador
- Equipo

## Jugador (Player)
- id: int
- name: str
- goals: int
- created_at: datetime
- updated_at: DateTime

## Equipo (Team)
- id: int
- name: str
- city: str
- created_at: datetime
- updated_at: datetime

## Requerimientos
- Se deben relacionar las entidades 
- Se debe hacer un `CRUD` de cada una de las entidades.
- Al consultar la información de un **equipo**, se deberá mostrar un valor adicional "`goals_count`" de sólo lectura que traerá el número de goles anotados por todos sus jugadores.
- Habilitar la opción para consultar por query parameters en los métodos `GET`.


# Instalacion
Este proyecto utiliza `Docker` para ser inicializado. Por otro lado, toda acción que se quiera realizar al servidor web se deberá hacer a través del archivo `Makefile` el cual continuee comandos utiles para interactuar con los contenedores de este proyecto.

## Fromscratch
Para iniciar el proyecto completamente desde cero, se puede utilizar el siguiente comando:

```
make fromscratch
```

## Atajos Básicos
Algunas acciones simples de `docker` se encuentran ya establecidas en el `Makefile`. Algunas de estas son:

```
make up         # similar to docker-compose up
make down       # similar to docker-compose down
make rebuild    # similar to docker-compose build
make reset      # deletes all docker volumes
```

# Otros Comandos
Hay muchos comandos utiles que pueden ser accedidos y utilizados desde el `Makefile`, para consultar sobre ellos puedes utilizar la opción `help`:

```
make help
```


# Colección de Peticiones
Dentro del repositorio, se encuentra el archivo `collection.json` el cual es una colección de peticiones de `ThunderClient` que puede ser utilizado para ejecutar los endpoints CRUD de *jugadores* y de *equipos*. Por desgracía, esta colección no es compatible con `Postman` y solo puede ser utilizado en `ThunderClient`; Favor considerar este factor al momento de importar este archivo.
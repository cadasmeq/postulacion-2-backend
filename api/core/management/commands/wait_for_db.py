import os
import sys
import time

import psycopg2
from django.core.management import BaseCommand


class Command(BaseCommand):
    """Django command to pause execution until db is available"""

    def handle(self, *args, **options):
        self.stdout.write("Waiting for database...")
        while True:
            try:
                POSTGRES_DB = os.getenv("POSTGRES_DB")
                POSTGRES_USER = os.getenv("POSTGRES_USER")
                POSTGRES_PASSWORD = os.getenv("POSTGRES_PASSWORD")
                POSTGRES_HOST = os.getenv("POSTGRES_HOST")
                POSTGRES_PORT = os.getenv("POSTGRES_PORT")

                psycopg2.connect(
                    dbname=POSTGRES_DB,
                    user=POSTGRES_USER,
                    password=POSTGRES_PASSWORD,
                    host=POSTGRES_HOST,
                    port=POSTGRES_PORT,
                )
            except psycopg2.OperationalError:
                self.stdout.write("Database unavailable, waititng 1 second...")
                time.sleep(1)
            else:
                self.stdout.write(self.style.SUCCESS("Database available!"))
                sys.exit(0)

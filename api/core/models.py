from django.db import models


class APIModel(models.Model):
    """API base model.
    APIModel acts as an abstract base class from which every
    other model in the project will inherit. This class provides
    every table with the following attributes:
        + created (DateTime): Store the datetime the object was created.
        + modified (DateTime): Store the last datetime the object was modified.
        + is_active(Bool): Stores activity state of a model
    """

    created = models.DateTimeField(
        "created at",
        auto_now_add=True,
        help_text="Date time on which the object was created.",
    )
    modified = models.DateTimeField(
        "modified at",
        auto_now=True,
        help_text="Date time on which the object was last modified.",
    )
    is_active = models.BooleanField(
        "activity model state",
        default=True,
        help_text="Boolean state of the object.",
    )

    class Meta:
        """Meta option."""

        abstract = True

        get_latest_by = "created"
        ordering = ["-created", "-modified"]

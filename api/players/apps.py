"""Players AppConfig"""
from django.apps import AppConfig


class PlayersAppConfig(AppConfig):
    """Players App Config"""

    name = "api.players"
    verbose = "Players"

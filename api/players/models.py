"""Player Model"""

from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

from api.core.models import APIModel


class Player(APIModel):
    """
    Player model

    Entity that represents a person that plays in a team.
    """

    id = models.AutoField(primary_key=True, serialize=False, verbose_name="ID")
    name = models.CharField("players name", max_length=150)
    goals = models.SmallIntegerField(
        "player goals",
        default=0,
        validators=[MinValueValidator(0), MaxValueValidator(999)],
    )

    def __str__(self):
        """returns the name of the player"""
        return self.name

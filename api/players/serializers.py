"""Players serializers"""

from rest_framework import serializers

from api.players.models import Player


class PlayerModelSerializer(serializers.ModelSerializer):
    """Players Model Serializer"""

    class Meta:
        """Meta class"""

        model = Player
        fields = ("name", "goals")

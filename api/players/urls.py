"""Player URLs"""

from django.urls import include, path
from rest_framework.routers import DefaultRouter

from api.players.views import PlayerViewset

router = DefaultRouter()
router.register(r"players", PlayerViewset, basename="players")


urlpatterns = [path("", include(router.urls))]

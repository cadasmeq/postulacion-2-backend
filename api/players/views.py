"""Player Views"""

from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, mixins, status, viewsets
from rest_framework.response import Response

from api.players.models import Player
from api.players.serializers import PlayerModelSerializer


class PlayerViewset(
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.ListModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    """
    Player Viewset

    Handles CRUD actions for Player Model
    """

    serializer_class = PlayerModelSerializer
    lookup_field = "pk"
    filter_backends = [DjangoFilterBackend, filters.SearchFilter]
    filterset_fields = ["name"]
    search_fields = ["name"]

    def get_queryset(self):
        """Restrict list to public-only."""

        queryset = Player.objects.all()
        if self.action == "list":
            return queryset.filter(is_active=True)
        return queryset

    def destroy(self, request, *args, **kwargs):
        """
        Instead of delete an instance, set its is_active field to false
        """

        player = self.get_object()
        player.is_active = False
        player.save()

        return Response(status=status.HTTP_204_NO_CONTENT)

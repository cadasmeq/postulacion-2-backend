"""Players AppConfig"""
from django.apps import AppConfig


class TeamsAppConfig(AppConfig):
    """Teams App Config"""

    name = "api.teams"
    verbose = "Teams"

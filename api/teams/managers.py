"""Team Manager"""

from django.db import models
from django.db.models import Sum


class TeamManager(models.Manager):
    """
    Team Manager

    this manager attachs some useful behavior
    that can be call ifn the Team model.
    """

    def count_goals(self, pk, **kwargs):
        """Count total goals of a team"""
        goals = self.get(pk=pk).players.aggregate(Sum("goals"))

        return goals["goals__sum"]

"""Player Model"""

from django.db import models

from api.core.models import APIModel
from api.teams.managers import TeamManager


class Team(APIModel):
    """
    Team model

    Entity that represents a group of players
    """

    MAX_TEAM_PLAYER = 20

    id = models.AutoField(primary_key=True, serialize=False, verbose_name="ID")
    name = models.CharField("teams name", max_length=150)
    city = models.CharField("teams city it belongs", max_length=150)

    players = models.ManyToManyField("players.Player", blank=True)

    # Manager
    objects = TeamManager()

    def __str__(self):
        """returns the name of the team"""
        return f"{self.name.capitalize()} From: {self.city.capitalize()}"

"""Team serializers"""

from rest_framework import serializers

from api.teams.models import Team


class TeamModelSerializer(serializers.ModelSerializer):
    """Team Model Serializer"""

    class Meta:
        """Meta class"""

        model = Team
        fields = ("name", "city")

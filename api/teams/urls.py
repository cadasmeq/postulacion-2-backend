"""Teams URLs"""

from django.urls import include, path
from rest_framework.routers import DefaultRouter

from api.teams.views import TeamViewset

router = DefaultRouter()
router.register(r"teams", TeamViewset, basename="teams")

urlpatterns = [path("", include(router.urls))]

"""Team Views"""

from django.shortcuts import get_object_or_404
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, mixins, status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from api.players.serializers import PlayerModelSerializer
from api.teams.models import Team
from api.teams.serializers import TeamModelSerializer


class TeamViewset(
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.ListModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    """
    Team Viewset

    Handles CRUD actions for Team Model
    """

    serializer_class = TeamModelSerializer
    lookup_field = "pk"
    filter_backends = [DjangoFilterBackend, filters.SearchFilter]
    filterset_fields = ["name", "city"]
    search_fields = ["name", "city"]

    @action(detail=True, methods=["post"])
    def add_player(self, request, *args, **kwargs):
        """
        Action allows add a new player into a certain Team.

        Next url must be use to add a new player:
        - url:
            /team/<int: pk>/add_player/

        - required payload:
            name (str)  : players name
            goals (int) : players goal
        """

        serializer = PlayerModelSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        player = serializer.save()

        team = get_object_or_404(Team, pk=kwargs.get("pk"))
        team.players.add(player)
        team.save()

        data = {
            "team": TeamModelSerializer(team).data,
            "players": PlayerModelSerializer(
                team.players.all(), many=True
            ).data,  # noqa
        }
        return Response(data, status=status.HTTP_201_CREATED)

    def get_queryset(self):
        """Restrict list to public-only."""
        queryset = Team.objects.all()
        if self.action == "list":
            return queryset.filter(is_active=True)
        return queryset

    def destroy(self, request, *args, **kwargs):
        """
        Instead of delete an instance, set its is_active field to false
        """
        team = self.get_object()
        team.is_active = False
        team.save()

        return Response(status=status.HTTP_204_NO_CONTENT)

    def retrieve(self, request, *args, **kwargs):
        """Updates the response in order to add total team goals"""

        # Fetch response from super class
        response = super(TeamViewset, self).retrieve(request, *args, **kwargs)
        data = {"team": response.data}

        # Get my team object
        team = self.get_object()

        # If my team contains members (players), then updates data response
        if team.players.count():
            goals = Team.objects.count_goals(pk=team.pk)
            players = PlayerModelSerializer(team.players.all(), many=True).data
            data = data | {"goals_count": goals, "players": players}

        return Response(data)

from django.urls import include, path  # noqa

urlpatterns = [
    path("", include(("api.teams.urls", "teams"), namespace="teams")),
    path("", include(("api.players.urls", "players"), namespace="players")),
]

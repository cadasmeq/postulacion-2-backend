# Dependencias Base
Para el desarrollo de este proyecto, se han utilizado diversas dependencias que permiten agilizar ciertos procesos que suelen ser imprescindible a la hora escribir codigo. Algunas de las herramientas que fueron utilizadas son las que se listan a continuación:

```
- django                : Framework para desarrollo web 
- djangorestframework   : Herramienta para desarrollo de APIs
- django-extensions     : Utilidades para django (ej. shell_plus)
- django-filter         : Herramienta para aplicar filtrado y busqueda en modelos de Django
```
# Dependencias de Desarrollo
Para el desarrollo de este proyecto, se han utilizado diversas dependencias que permiten agilizar ciertos procesos que suelen ser imprescindible a la hora escribir codigo. Algunas de las herramientas que fueron utilizadas son las que se listan a continuación:

```
- flake8        : Herramienta para inspección de código estatico
- pytest        : Herramienta de testing de código.
- pytest-django : Complemento de pytest que agrega funcionalidades en Django
- pre-commit    : Libreria que permite ejecutar hooks antes de cada commit
- black         : Herramienta para formateo de código automatico
- isort         : Herramienta que permite ordenar automaticamente las importaciones.
```
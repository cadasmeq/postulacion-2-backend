# About this App
This software uses Django as framework to run a new server that serves a set of APIs.


## API Folder
Whole app is located in `api/` which contains mostly every single file to run the django server. The only except is `manage.py` which is located in root path of this project. 

```
api/
├── settings.py     : app settings
├── asgi.py         : django file
└── wsgi.py         : django file
├── urls.py         : root from all urls
├── core/           : core folder is a django app that contains useful utilities for developers
├── players/        : players folder is a django app that holds every logic about this players entity
├── teams/          : teams folder is a django app that holds every logic about this teams entity
```

## Tests Folders
Besides, there is a `tests/` folders in root path of this project, which contains every test associated a each implementation of `api/`. 
```
tests
├── conftest.py     : file that stores every fixture that pytest should use
├── e2e             : folder that contains every end-to-end tests.
├── integration     : folder that contains integretion test.
└── unit            : folder that contains unit tests.
```

## Infraestructure
- Postgres: Its used postgres as storage which which dockerized in this project
- Django: Its used Django to serve REST APIs which is dockerized int this project
- Redis: Its used to serve as NOSQL Database, being able to be used such as event bus or storage.

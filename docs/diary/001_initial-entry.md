# Estrategia de Desarrollo
Se ha optado por utilizar TDD para realizar el desarrollo de los presentes requerimientos utilizado el algoritmo Red Gren Refactor. 


## Tareas
se ha desglosado el requerimiento en multiples tareas ordenadas por grado de dificultad y tiempo para desarrollarlas, siendo las primeras las mas rápidas y sencillas, y últimas la mas complicadas o lentas de desarrollar.

* Crear modelos de las entidades Jugador y Equipo son sus respectiva relacion
* Documentar modelo entidad relacion
* Crear un viewsets tipo `CRUD` por cada entidad
* Agregar atributo `goals_count` (solo lectura) al consultar un **equipo**
* Habilitar opcion para consultar por query parameters en los metodos `GET`

## Consideraciones Personales
Se realizaran las siguientes consideraciones a la hora de diseñar el sistema:

* Crear modelo base abstracto de los cuales heredaran todas las demas entidades
* Esto puede realizarse a nivel de base de datos (agregando esto como una nueva columna)
* Esto tambien puede realizarse al modificar el response final al hacer retrieve de un `goals_count`
* Viewsets ya traen esta implementación por defecto habilitado.
* Como opción se agregaran query_params que suelen ser acciones de paginacion o filtrado de una lista de resultados


# Tests
## Unit Testing
- Testear creacion de modelo de **Jugador**
- Testear creacion de modelo de **Equipo**

## Integration Testing
- Testear relacion de los modelos de **Equipo** y **Jugador**
- Testear integración en endpoint que agrega un contador de goles valido.

## E2E Testing
- Realizar testing a cada accion del CRUD **Jugador**
- Realizar testing a cada accion del CRUD **Equipo**
- Realizar testing en accion retrieve que contenga ``goals_count``
- Realizar testing que desmuestre que es posible realizar `query_params` para acciones de paginado o filtrado

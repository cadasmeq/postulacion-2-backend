# Diario de Desarrollo

## Tarea 
Crear modelos de las entidades Jugador y Equipo son sus respectiva relacion. 

## Estado
Finalizada

## Observaciones
- Se crea modelo abstracto que permite identificar la creacion y ultima actualización de todos los modelos que lo hereden.
- Se crean modelos con la estructura indicada en requerimiento.
- Se genera relizacion ManyToMany de Player con Team, estableciendole una validación sobre el maximo miembros por equipo

## Siguiente Tarea
- Generar pruebas unitarias para ambos modelos.
- Generar documento que grafique la relación de ambos modelos.

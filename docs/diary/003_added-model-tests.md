# Diario de Desarrollo

## Tarea 
- Testear creacion de modelo de **Jugador**
- Testear creacion de modelo de **Equipo**
- Testear relacion de los modelos de **Equipo** y **Jugador**

## Estado
Finalizada

## Observaciones
- Se crean test para cada uno de los casos.

## Siguiente Tarea
- Se iniciará creación de CRUD para cada modelo

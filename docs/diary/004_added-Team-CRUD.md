# Diario de Desarrollo

## Tarea 
- Crear un viewsets tipo `CRUD` por cada entidad
- Realizar testing a cada accion del CRUD **Equipo**

## Estado
Finalizada

## Observaciones
- Se crea un CRUD simple para el modelo `Team` con sus respectivos tests End to End.

## Siguiente Tarea
- Se iniciará creación de CRUD para el modelo `Player`

# Diario de Desarrollo

## Tarea 
- Crear un viewsets tipo `CRUD` por cada entidad
- Realizar testing a cada accion del CRUD **Jugador**

## Estado
Finalizada

## Observaciones
- Se crea un CRUD simple para el modelo `Player` con sus respectivos tests End to End.

## Siguiente Tarea
- Se continuará con tarea de agregar atributo `goals_count` (solo lectura) al consultar un **equipo**

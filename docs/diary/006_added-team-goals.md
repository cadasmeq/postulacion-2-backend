# Diario de Desarrollo

## Tarea 
- Se continuará con tarea de agregar atributo `goals_count` (solo lectura) al consultar un **equipo**
- Testear integración en endpoint que agrega un contador de `goals`

## Estado
Finalizada

## Observaciones
- Se agrega manager para Team que permite calcular `goals_count` de cada `Team`
- Se modifica implementacion de `retrieve()` desde el viewset de `Team` para que el response contenga `goals_count` y los `players` del equipo. 

## Siguiente Tarea
- Habilitar opcion para consultar por query parameters en los metodos `GET`

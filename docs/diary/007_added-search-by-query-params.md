# Diario de Desarrollo

## Tarea 
- Habilitar opcion para consultar por query parameters en los metodos `GET`
- Realizar testing que desmuestre que es posible realizar `query_params` para acciones de paginado o filtrado

## Estado
Finalizada

## Observaciones
- Se implementa Django Filter Backend para ambos CRUD de modo que se pueda filtrar los resultados de tipo `GET` utilizando `query_params`
- Se implementa testing end-to-end para los filtros por `query_params`

## Siguiente Tarea
- Facilitar modo de agregar *jugadores* a los *equipos* utilizando los endpoints

# Diario de Desarrollo

## Tarea 
- Implementar una manera de agregar nuevos *jugadore* a un *equipo*

## Estado
Finalizada

## Observaciones
- Se implementa action de tipo `POST` en `TeamViewset` que permite ingresar un nuevo players por medio de la accion `add_player`.

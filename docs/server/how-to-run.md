# How To Run
This project requires `Docker` and `Docker Compose` to be initialized. Furthermore, this app can be accessed easily using the Makefile which contains usefuls commands that allows you intereact directly with its containers to run the app. 

## Fromscratch
To start run a new server completly from scratch, you could use:

```
make fromscratch
```

## Shortcuts
Some base docker actions such as up, down, build a container are declared in the Makefile in order ease complex actions. you coud use:

```
make up         # similar to docker-compose up
make down       # similar to docker-compose down
make rebuild    # similar to docker-compose build
```

There are more commands that you can accessed consulting `help` option:

```
make help
```
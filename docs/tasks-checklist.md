# Lista de Tareas

[x] Crear modelos de las entidades Jugador y Equipo son sus respectiva relacion
[x] Documentar modelo entidad relacion
[x] Crear un viewsets tipo `CRUD` por cada entidad
[x] Agregar atributo `goals_count` (solo lectura) al consultar un **equipo**
[x] Habilitar opcion para consultar por query parameters en los metodos `GET`


# Tests
## Unit
[x] Testear creacion de modelo de **Jugador**
[x] Testear creacion de modelo de **Equipo**

## Integration
[x] Testear relacion de los modelos de **Equipo** y **Jugador**
[x] Testear integración en endpoint que agrega un contador de `goals`

## E2E
[x] Realizar testing a cada accion del CRUD **Jugador**
[x] Realizar testing a cada accion del CRUD **Equipo**
[x] Realizar testing en accion retrieve que contenga ``goals_count``
[x] Realizar testing que desmuestre que es posible realizar `query_params` para acciones de paginado o filtrado

"""Test fixtures"""

from api.players.models import Player
from api.teams.models import Team


def new_player(name: str, goals: int):
    """Returns a new instance of Player"""
    return Player.objects.create(name=name, goals=goals)


def new_team(name: str, city: str):
    """Returns a new instance of Team"""
    return Team.objects.create(name=name, city=city)


def validate_team_response(response, **kwargs):
    """Helps to validate response from Team Endpoint

    Args:
        response ([Response]): Object that contains a response from teams API
        kwargs ([Dict]):
            name ([str]): Expected name of the team
            city ([str]): Expected city of the team
            status_code ([int]): Expected status code from the response
    """

    expected_status_code = kwargs.setdefault("status_code", None)
    expected_team_name = kwargs.setdefault("name", None)
    expected_team_goals = kwargs.setdefault("city", None)

    assert response.status_code == expected_status_code
    assert response.data["name"] == expected_team_name
    assert response.data["city"] == expected_team_goals


def validate_player_response(response, **kwargs):
    """Helps to validate response from Player Endpoint

    Args:
        response ([Response]): Object that contains a response from players API
        kwargs ([Dict]):
            name ([str]): Expected name of the player
            goals ([int]): Expected goals of the player
            status_code ([int]): Expected status code from the response
    """

    expected_status_code = kwargs.setdefault("status_code", None)
    expected_team_name = kwargs.setdefault("name", None)
    expected_team_goals = kwargs.setdefault("goals", None)

    assert response.status_code == expected_status_code
    assert response.data["name"] == expected_team_name
    assert response.data["goals"] == expected_team_goals

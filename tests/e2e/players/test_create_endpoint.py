"""Test for creation of new players"""

import pytest
from django.urls import reverse

from api.players.models import Player
from tests.conftest import validate_player_response


@pytest.mark.django_db
@pytest.mark.parametrize(
    "name,goals",
    [
        ("Messi", 0),
        ("Salas", 107),
        ("Maradona", 376),
    ],
)
def test_create_player_using_its_endpoint(client, name, goals):
    """Test creation of a new player using its endpoint"""

    # defines my url
    url = reverse("players:players-list")

    # make request
    response = client.post(url, data={"name": name, "goals": goals})

    # define a dictionary about what my response must contain
    expected_values = {
        "status_code": 201,
        "name": name,
        "goals": goals,
    }

    # validates my player response
    validate_player_response(response=response, **expected_values)

    # assert that my player was created successfuly
    assert Player.objects.count() == 1

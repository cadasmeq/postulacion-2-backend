"""Test for delete players"""

import pytest

from api.players.models import Player
from tests.conftest import new_player


@pytest.mark.django_db
@pytest.mark.parametrize(
    "name,goals",
    [
        ("Messi", 0),
        ("Salas", 107),
        ("Maradona", 376),
    ],
)
def test_delete_player_using_its_endpoint(client, name, goals):
    """Test delete endpoints works correctly for existents players"""

    # Creates a new player in the db
    player = new_player(name=name, goals=goals)

    # defines url
    url = f"/players/{player.pk}/"

    # make request
    response = client.delete(url)

    # assert correct status code and its inactive
    assert response.status_code == 204
    assert not Player.objects.get(pk=player.pk).is_active

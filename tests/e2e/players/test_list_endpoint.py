"""Test listing action of all players"""

import pytest
from django.urls import reverse

from api.players.models import Player
from tests.conftest import new_player


@pytest.mark.django_db
def test_list_players_using_its_endpoint(client):
    """Test endpoint that list all available players"""

    # Creates a new player on db
    new_player(name="Messi", goals=0)
    new_player(name="Salas", goals=107)
    new_player(name="Maradona", goals=367)

    # defines url
    url = reverse("players:players-list")

    # make request
    response = client.get(url)

    # Validates if total of created players are the same in the db
    players_obtained = response.data
    assert len(players_obtained) == Player.objects.count()

    # Validates if data from created players exists in the db
    for player in players_obtained:
        assert Player.objects.filter(**player).exists()

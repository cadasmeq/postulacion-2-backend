"""Test for partial update players"""

import json
from random import randint

import pytest

from tests.conftest import new_player, validate_player_response


@pytest.mark.django_db
@pytest.mark.parametrize(
    "name,goals",
    [
        ("Messi", 0),
        ("Salas", 107),
        ("Maradona", 376),
    ],
)
def test_patch_player_using_its_endpoint(client, name, goals):
    """Test endpoint that partial update correctly a player information"""

    # Creates a new player on db
    player = new_player(name=name, goals=goals)

    # defines url
    url = f"/players/{player.pk}/"

    # Create a new payload about what we want to partial update
    new_goals = randint(0, 100)
    payload = json.dumps({"goals": new_goals})

    # make request
    response = client.patch(
        url,
        data=payload,
        content_type="application/json",
    )

    # define a dictionary about what my response must contain
    expected_values = {
        "status_code": 200,
        "name": name,
        "goals": new_goals,
    }
    validate_player_response(response=response, **expected_values)

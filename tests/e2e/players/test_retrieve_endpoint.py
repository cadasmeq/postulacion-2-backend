"""Test retrieve action from Player CRUD"""

import pytest

from tests.conftest import new_player, validate_player_response


@pytest.mark.django_db
@pytest.mark.parametrize(
    "name,goals",
    [
        ("Messi", 0),
        ("Salas", 107),
        ("Maradona", 376),
    ],
)
def test_retrieve_player_using_its_endpoint(client, name, goals):
    """Test endpoint that retrieves a player using its pk"""

    # Creates a new player in the db
    player = new_player(name=name, goals=goals)

    # defines url
    url = f"/players/{player.pk}/"

    # make request
    response = client.get(url)

    # define a dictionary about what my response must contain
    expected_values = {
        "status_code": 200,
        "name": name,
        "goals": goals,
    }

    # define a dictionary about what my response must contain
    validate_player_response(response=response, **expected_values)

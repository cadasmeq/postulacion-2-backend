"""Test listing players and searching it by query params"""

import pytest
from django.urls import reverse

from api.players.models import Player
from tests.conftest import new_player


@pytest.mark.django_db
def test_list_players_using_search_query_params(client):
    """Test endpoint that list all available players"""

    # Creates a new player on db
    new_player(name="Marcelo Rios", goals=367)
    new_player(name="Marcelo Lagos", goals=273)

    # defines url
    url = reverse("players:players-list")

    # Search every player that its name starts with 'marcelo'
    MARCELO = "marcelo"
    query_params = {"search": MARCELO}
    response = client.get(url, query_params)

    # Validates results found using search query param
    marcelos = Player.objects.filter(name__contains=MARCELO)
    assert len(response.data) == marcelos.count()

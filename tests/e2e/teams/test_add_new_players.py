"""E2E Test to add new players in a certain team"""

import pytest

from tests.conftest import new_team


@pytest.mark.django_db
@pytest.mark.parametrize("name,goals", [("Michel Jackson", 100)])
def test_create_team_using_its_endpoint(client, name, goals):
    """Test creation of a new team using its endpoint"""

    team = new_team(name="test", city="test")

    # defines my url
    url = f"/teams/{team.pk}/add_player/"

    # make request
    response = client.post(url, data={"name": name, "goals": goals})
    assert response.status_code == 201

    team_data = response.data["team"]
    assert team_data["name"] == team.name
    assert team_data["city"] == team.city

    players_data = response.data["players"]
    for player in players_data:
        assert player["name"] == name
        assert player["goals"] == goals

"""Test for create news teams"""

import pytest
from django.urls import reverse

from api.teams.models import Team
from tests.conftest import validate_team_response


@pytest.mark.django_db
@pytest.mark.parametrize("name,city", [("Juventus", "Turin")])
def test_create_team_using_its_endpoint(client, name, city):
    """Test creation of a new team using its endpoint"""
    # defines my url
    url = reverse("teams:teams-list")

    # make request
    response = client.post(url, data={"name": name, "city": city})

    # define a dictionary about what my response must contain
    expected_values = {
        "status_code": 201,
        "name": name,
        "city": city,
    }

    # validates my team response
    validate_team_response(response=response, **expected_values)

    # assert that my team was created successfuly
    assert Team.objects.count() == 1

"""Test for delete an existing team"""

import pytest

from api.teams.models import Team
from tests.conftest import new_team


@pytest.mark.django_db
@pytest.mark.parametrize("name,city", [("Juventus", "Turin")])
def test_delete_team_using_its_endpoint(client, name, city):
    """Test delete endpoints works correctly for existents teams"""

    # Creates a new team in the db
    team = new_team(name=name, city=city)

    # defines url
    url = f"/teams/{team.pk}/"

    # make request
    response = client.delete(url)

    # assert correct status code and its inactive
    assert response.status_code == 204
    assert not Team.objects.get(pk=team.pk).is_active

"""Test List action from Team CRUD"""

import pytest
from django.urls import reverse

from api.teams.models import Team
from tests.conftest import new_team


@pytest.mark.django_db
def test_list_teams_using_its_endpoint(client):
    """Test endpoint that list all available teams"""

    # Creates a new team on db
    new_team(name="barcelona", city="testlandia")
    new_team(name="manchester", city="testlandia")
    new_team(name="audax", city="testlandia")

    # defines url
    url = reverse("teams:teams-list")

    # make request
    response = client.get(url)

    # Validates if total of created teams are the same in the db
    teams_obtained = response.data
    assert len(teams_obtained) == Team.objects.count()

    # Validates if data from created teams exists in the db
    for team in teams_obtained:
        assert Team.objects.filter(**team).exists()

"""Test for partial update an existing team"""

import json

import pytest

from tests.conftest import new_team, validate_team_response


@pytest.mark.django_db
@pytest.mark.parametrize("name,city", [("Juventus", "Turin")])
def test_patch_team_using_its_endpoint(client, name, city):
    """Test endpoint that partial update correctly a team information"""
    # Creates a new team on db
    team = new_team(name=name, city=city)

    # defines url
    url = f"/teams/{team.pk}/"

    # Create a new payload about what we want to partial update
    new_city = "Santiago de Chile"
    payload = json.dumps({"city": new_city})

    # make request
    response = client.patch(
        url,
        data=payload,
        content_type="application/json",
    )

    # define a dictionary about what my response must contain
    expected_values = {
        "status_code": 200,
        "name": name,
        "city": new_city,
    }
    validate_team_response(response=response, **expected_values)

"""Tetst Retrieve Action from Team CRUD"""

import pytest

from tests.conftest import new_team


@pytest.mark.django_db
@pytest.mark.parametrize("name,city", [("Juventus", "Turin")])
def test_retrieve_team_using_its_endpoint(client, name, city):
    """Test endpoint tha retrieves a team using its pk"""

    # Creates a new team in the db
    team = new_team(name=name, city=city)

    # defines url
    url = f"/teams/{team.pk}/"

    # make request
    response = client.get(url)
    assert response.status_code == 200

    # Validates returned data from response
    team_data = response.data["team"]
    assert team_data["name"] == name
    assert team_data["city"] == city

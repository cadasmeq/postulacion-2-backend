"""Test listing players and searching it by query params"""

import pytest
from django.urls import reverse

from api.teams.models import Team
from tests.conftest import new_team


@pytest.mark.django_db
def test_list_teams_using_search_query_params(client):
    """Test endpoint that list all available teams"""

    # Creates a new team on db
    new_team(name="Audax", city="Santiago")
    new_team(name="Santiago Morning", city="Santiago")

    # defines url
    url = reverse("teams:teams-list")

    # Search every teams that its city starts with 'santiago'
    SANTIAGO = "santiago"
    query_params = {"search": SANTIAGO}
    response = client.get(url, query_params)

    # Validates results found using search query param
    santiagos = Team.objects.filter(city__contains=SANTIAGO)
    assert len(response.data) == santiagos.count()

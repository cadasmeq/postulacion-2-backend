"""Test team goals information"""

from random import randint

import pytest

from api.teams.models import Team
from tests.conftest import new_player, new_team


@pytest.mark.django_db
def test_retrieve_response_must_return_team_goals(client):
    """
    When its wanted to retrieve information about a team,
    the response must return the total goals from the whole team.
    """

    players = [
        new_player(name="Messi", goals=randint(1, 100)),
        new_player(name="Sanchez", goals=randint(1, 100)),
        new_player(name="Maradona", goals=randint(1, 100)),
    ]

    my_team = new_team(name="Test", city="Test")
    my_team.players.add(*players)
    my_team.save()

    url = f"/teams/{my_team.pk}/"
    response = client.get(url)

    goals = Team.objects.count_goals(pk=my_team.pk)
    assert response.status_code == 200
    assert response.data["goals_count"] == goals

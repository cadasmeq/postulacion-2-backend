"""Test Player Model"""

import pytest

from api.players.models import Player
from tests.conftest import new_player


@pytest.mark.django_db
@pytest.mark.parametrize(
    "name,goals",
    [
        ("Dieguito Maradona", 346),
        ("Marcelito Salas", 157),
    ],
)
def test_player_creation(name, goals):
    """Test Player Model"""

    _ = new_player(name=name, goals=goals)
    assert Player.objects.count() == 1

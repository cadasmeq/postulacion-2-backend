import pytest

from tests.conftest import new_player, new_team


@pytest.mark.django_db
def test_add_players_to_teams():
    """Test relationship of players team and how they are integrated"""

    barcelona = new_team(name="Barcelona F.C.", city="Barcelona")

    messi = new_player(name="Messi", goals=0)
    sanchez = new_player(name="Sanchez", goals=5)
    medel = new_player(name="Medel", goals=2)

    barcelona.players.add(messi)
    barcelona.players.add(sanchez)
    barcelona.players.add(medel)
    barcelona.save()

    assert barcelona.players.count() == 3

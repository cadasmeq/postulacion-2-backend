"""Test Player Model"""

import pytest

from api.teams.models import Team
from tests.conftest import new_team


@pytest.mark.django_db
@pytest.mark.parametrize(
    "name,city",
    [
        ("Universidad de Chile", "Santiago"),
        ("Real Madrid", "Madrid"),
    ],
)
def test_teams_creation(name, city):
    """Test Teams Model"""

    _ = new_team(name=name, city=city)
    assert Team.objects.count() == 1
